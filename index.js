const express = require('express');
const multer = require('multer');
const upload = multer({ dest: 'uploaded-files/' });
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const mysql = require('mysql2');
const app = express();
let db = require("./db");
const appRoute = require("./routes/app");
const port = 4000;
const saltRounds = 12;
app.use(express.json());
const postRoute = require("./routes/posts");
const commentRoute = require("./routes/comments");
const memberRoute = require("./routes/members");
app.use("/uploaded-files", express.static('uploaded-files'));
app.use("/uploaded-picture",express.static("uploaded-picture"));
app.use("/",appRoute);
app.use("/posts",postRoute);
app.use("/comments",commentRoute);
app.use("/members",memberRoute);











  
  
  
  
  app.listen(port, () => {
    console.log("Example app listening on port 3000!");
    const mysqlPool = mysql.createPool({
      host: 'localhost',
      port: 3306,
      user: 'root',
      password: 'root',
      database: 'lihkg'
    });
   db.setDb(mysqlPool.promise());
  });

const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();
const router = express.Router();
let secret = "member";

app.use(express.json());

let verify = function (req, res, next) {
    let token = req.headers["authorization"].split(" ")[1];
    jwt.verify(token, secret, (err, decoded) => {
        if (decoded !== null) {
           req.userEmail =  decoded.email;
           req.memberId = decoded.memberId;
           return next();
        }else{
            return res.json({
                "success":false,
                "message": "token is not valid"
            })
        }
    })
}

module.exports = verify;
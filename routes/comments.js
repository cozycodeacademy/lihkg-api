const express = require("express");
const app = express();
app.use(express.json());
const router = express.Router();
const multer = require("multer");
const upload = multer({ dest: "uploaded-files/" });
const db = require("../db.js").getDb;
const xss = require("xss");
const verify = require("../tools/verify");

router.post("/", verify, async (req, res) => {
  let lt = /</g,
    gt = />/g,
    ap = /'/g,
    ic = /"/g;
  let memberId = parseInt(req.memberId);
  let date = new Date();
  let content = req.body.content
    .replace(lt, "&lt;")
    .replace(gt, "&gt;")
    .replace(ap, "&#39;")
    .replace(ic, "&#34;");
  let postId = req.body.postId;
  let likeCount = 0;
  let disCount = 0;
  let private = req.body.private;

  try {
    if (content.split(" ").length > 100) {
      return res.json({
        success: false,
        message: "sorry your content is too long",
      });
    } else {
      const [result, fields] = await db().execute(
        "insert into comment(commentor,date,content,post_id,like_count,dis_count,private)values(?,?,?,?,0,0,?)",
        [memberId, date, content, postId, private]
      );
      res.json({ succuse: true, result });
    }
  } catch (error) {
    res.json({ succuse: false, message: "sorry you can't comment" });
    console.log(error);
  }
});

router.post(
  "/:commentId/photo",
  verify,
  upload.array("picture", 5),
  async (req, res) => {
    try {
      const memberId = parseInt(req.memberId);
      let files = req.files;
      console.log(files);
      let commentId = req.params.commentId;
      for (let i = 0; i < files.length; i++) {
        if (!files[i].originalname.match(/\.(png|jpg|webp|gif)$/)) {
          return res.json({
            success: false,
            message: "your files format wrong",
          });
        } else {
          let str = req.files[i].path;
          let newStr = "/" + str.replace(/\\/g, "/");
          const [result, fields] = await db().execute(
            "insert into comment_picture(path,comment_id)values(?,?)",
            [newStr, commentId]
          );
          return res.json({ success: true, result: result });
        }
      }
    } catch (error) {
      console.log(error);
      res.json({ success: false, message: "sorry you can't post photo " });
    }
  }
);

router.post("/:commentId/dislike", verify, async (req, res) => {
  try {
    const commentId = req.params.commentId;
    const memberId = parseInt(req.memberId);
    // const memberId = 1;
    console.log(req.memberId);
    //find comment
    async function findComment() {
      const [commentIsThere, fields1] = await db().execute(
        `select * from comment where comment_id = ?`,
        [commentId]
      );
      if (commentIsThere.length > 0) {
        return true;
      } else {
        return false;
      }
    }
    const commentIsFound = await findComment(commentId);
    if (!commentIsFound) {
      return res.json({
        success: false,
        message: "the comment is not found",
      });
    }
    //confirm the comment is liked
    const [isLike, fields2] = await db().execute(
      `select * from member_comment_like where like_comment_id=? and like_member_id=? and islike=true`,
      [commentId, memberId]
    );
    if (isLike.length > 0) {
      return res.json({
        success: false,
        message: "the comment is liked, so you should cancel the like first",
      });
    }
    //confirm the comment is disliked
    const [isDislike, fields3] = await db().execute(
      `select * from member_comment_like where like_comment_id=? and like_member_id=? and islike=false`,
      [commentId, memberId]
    );
    if (isDislike.length === 0) {
      //dislike the comment and add a dislikeCount
      const [dislike, fields6] = await db().execute(
        `insert into member_comment_like (like_comment_id, like_member_id, islike) values(?, ?, false)`,
        [commentId, memberId]
      );
      const [addOneDislike, fields7] = await db().execute(
        `update comment set dis_count = dis_count+1 where comment_id = ?`,
        [commentId]
      );
      if (dislike.affectedRows > 0) {
        return res.json({
          success: true,
          message: "success to dislike the comment",
        });
      } else {
        return res.json({
          success: false,
          message: "fail to dislike the comment",
        });
      }
    } else {
      // unDislike the comment and reduce a dislikeCount
      const [unDislike, fields8] = await db().execute(
        `delete from member_comment_like where like_comment_id=? and like_member_id=? and islike=false`,
        [commentId, memberId]
      );
      const [reduceOneDislike, fields9] = await db().execute(
        `update comment set dis_count = dis_count-1 where comment_id = ?`,
        [commentId]
      );
      if (unDislike.affectedRows > 0) {
        return res.json({
          success: true,
          message: "success to unDislike the comment",
        });
      } else {
        return res.json({
          success: false,
          message: "fail to unDislike the comment",
        });
      }
    }
  } catch (error) {
    console.log(error);
    res.json(error);
  }
});

router.post("/:commentId/like", verify, async (req, res) => {
  try {
    const commentId = parseInt(req.params.commentId);
    const memberId = parseInt(req.memberId);
    const [result] = await db().execute(
      "select * from comment where comment_id=?",
      [commentId]
    );
    if (result.length > 0) {
      const [commentLike] = await db().execute(
        "select * from member_comment_like where like_comment_id=?",
        [commentId]
      );

      if (commentLike.length === 0) {
        const [setLike] = await db().execute(
          "insert into member_comment_like(like_comment_id,like_member_id,islike)values(?,?,?)",
          [commentId, memberId, 1]
        );
        const [updateComment] = await db().execute(
          "update comment set like_count=like_count+1 where comment_id=?",
          [commentId]
        );
        res.json({
          success: true,
          message: "add like success",
        });
      } else if (commentLike.length > 0 && commentLike[0].islike == 1) {
        const [cancelLike] = await db().execute(
          "delete from member_comment_like where like_comment_id=?",
          [commentId]
        );
        const [deleteLike] = await db().execute(
          "update comment set like_count=like_count-1 where comment_id=? and like_count!=0",
          [commentId]
        );
        res.json({
          success: true,
          message: "cancel like success",
        });
      } else {
        res.json({
          success: false,
          message: "you have to cancel the dislike first",
        });
      }
    } else {
      res.json({
        success: false,
        message: "no this comment",
      });
    }
  } catch (error) {
    console.log(error);
    res.json({ success: false, message: "can't find this comment" });
  }
});

router.get("/:postId/:memberId/:page", verify, async (req, res) => {
  try {
    const postId = parseInt(req.params.postId);
    const page = parseInt(req.params.page);
    const pageNum = (page - 1) * 15;
    const memberId = parseInt(req.params.memberId);
    let currentTime = new Date().getTime();
    // const [result] = await db().execute(
    //   "select * from comment where post_id=? and commentor =? limit ?,15",
    //   [postId, memberId, pageNum + ""]
    // );
    // const [result] = await db().execute(
    //   "select * from comment c inner join member m on m.member_id= c.commentor inner join comment_picture cp on cp.comment_id= c.comment_id where c.post_id=? and c.commentor=? limit ?, 15",
    //   [postId, memberId, pageNum + ""]
    // );

    const comment_sql = `select m.username, c.* from comment c
        left join member m on m.member_id = c.commentor
        where c.post_id = ${postId} and c.commentor = ${memberId} order by date asc limit 15 offset ${pageNum}`;
      const [commentResult, commentfields] = await db().execute(comment_sql);

      const comment_pic_sql = `with temp as (select comment_id from comment 
            where post_id = ${postId} and commentor = ${memberId} order by date asc limit 15 offset ${pageNum})
            select cp.* from comment_picture cp
            inner join temp on temp.comment_id = cp.comment_id`;
      const [commentPicResult, commentPicfields] = await db().execute(
        comment_pic_sql
      );
      let ary = [];
      for (let i = 0; i < commentPicResult.length; i++) {
        let found = ary.findIndex(
          (e) => e.comment_id === commentPicResult[i].comment_id
        );
        if (found > -1) {
          ary[found].path.push(commentPicResult[i].path.replace(/\\/g,"/"));
        } else {
          ary.push({
            comment_id: commentPicResult[i].comment_id,
            path: [commentPicResult[i].path.replace(/\\/g,"/")],
          });
        }
      }

      for (let i = 0; i < commentResult.length; i++) {
        let found = ary.findIndex(
          (e) => e.comment_id === commentResult[i].comment_id
        );
        if (found > -1) {
          commentResult[i]["pic_path"] = ary[found].path;
        } else {
          commentResult[i]["pic_path"] = null;
        }
      }
    
    const [countPage] = await db().execute(
      "select count (*) AS countpage from comment where post_id=? and commentor=?",
      [postId, memberId]
    );
    let totalPage = Math.ceil(countPage[0].countpage / 15);

   
    res.json({
      success: true,
      comment: commentResult,
      totalPage: totalPage,
    });
  } catch (err) {
    res.json({
      success: false,
      message: "faied",
    });
    console.log(err);
  }
});

module.exports = router;

// "select * from comment inner join member on member_id=commentor where post_id=? and commentor=?  limit ?, 15",

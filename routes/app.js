const express = require('express');
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const mysql = require('mysql2');
const app = express();
const port = 3000;
const saltRounds = 12;
app.use(express.json());
const router = express.Router();
const db = require("../db.js").getDb;




router.post("/register", async (req, res) => {
  let userName = req.body.userName;
  let password = req.body.password;
  let email = req.body.email;
  let pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;


  try {
    if (userName.length <1) {
      return res.json({ success: false, message: "sorry your userName is too short" });
    }
     else if (password.length <8) {
      return res.json({ success: false, message: "sorry your password is too short" });
    }
     else if (!email.match(pattern)) {
      return res.json({ success: false, message: "sorry your your email format is worng" });
    }else{
          let hash = await bcrypt.hash(password, saltRounds);
    const [result, fields] = await db().execute("insert into member(username,password,email)values(?,?,?)", [userName, hash, email]);
    console.log(result);
    const payload = {
      "email": email,
      "memberId":result.insertId
    };
    const token = jwt.sign(payload, "member");
    console.log(token);
    return res.json({ success: true, token: token, })
    }
    
  } catch (error) {
    console.log(error);
  }
});


router.post("/login", async (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  
  try {
    const [result, fields] = await db().execute("select*from member where email=?", [email]);
    if (result.length === 1) {
      console.log(result);
      bcrypt.compare(password, result[0].password).then((correct) => {
        if (correct) {
          const payload = {
            "email": email,
            "memberId":result[0].member_id
          };
          const token = jwt.sign(payload, "member");
          console.log(token);
          return res.json({ success: true, token: token })

        } else {
          res.json({ success: false, message: "password is not correct"})
        }
      })
    }else{
      res.json({ success: false, message:"acc not found"})
    }
  } catch (error) {
    console.log(error)
  }
})










module.exports = router;




const express = require("express");
const router = express.Router();
const db = require("../db").getDb;
const verify = require("../tools/verify");

router.patch("/profile", verify, async (req, res) => {
  try {
    const newName = req.body.newName;
    const memberId = parseInt(req.member.id);
    // const memberId = 9;

    //check new name length
    function correctNewNameLength() {
      if (newName.length >= 1) {
        return true;
      } else {
        return false;
      }
    }
    let isCorrectNewNameLength = correctNewNameLength(newName);
    if (!isCorrectNewNameLength) {
      return res.json({
        success: false,
        message: "username characters should at least 1 characters",
      });
    }
    //check username is being use by you
    async function beingUseByYou() {
      const [beingUseNameByYou, fields] = await db().execute(
        `select * from member where member_id =? and username=? `,
        [memberId, newName]
      );
      if (beingUseNameByYou.length > 0) {
        return true;
      } else {
        return false;
      }
    }
    let isBeingUseByYou = await beingUseByYou(memberId, newName);
    if (isBeingUseByYou) {
      return res.json({
        success: false,
        message: "the user name is being use by you",
      });
    }
    //check username is being use by others
    async function beingUseByOthers() {
      const [beingUseNameByOthers, field2] = await db().execute(
        `select * from member where member_id !=? and username=?`,
        [memberId, newName]
      );
      if (beingUseNameByOthers.length > 0) {
        return true;
      } else {
        return false;
      }
    }
    let isBeingUseByOthers = await beingUseByOthers(memberId, newName);
    if (isBeingUseByOthers) {
      return res.json({
        success: false,
        message: "the user name is being use by others",
      });
    }
    //update username
    async function rename() {
      const [rename, fields3] = await db().execute(
        `update member set username = ? where member_id = ?`,
        [newName, memberId]
      );
      if (rename.affectedRows > 0) {
        res.json({
          success: true,
          message: "your username is changed",
        });
      } else {
        res.json({
          success: false,
          message: "fail to change username",
        });
      }
    }
    rename(newName, memberId);
  } catch (error) {
    console.log(error);
    res.json(error);
  }
});

router.post("/history/:postId", verify, async (req, res) => {
  try {
    let memberId = req.memberId;
    let postId = req.params.postId;
    const [findRecordResult, findRecordFields] = await db().execute(
      "select * from history where history_post_id =? and history_member_id = ?",
      [postId, memberId]
    );
    if (findRecordResult.length === 0) {
      const [insertResult, insertFields] = await db().execute(
        "insert into history (history_post_id, history_member_id, history_date) values (?,?,now())",
        [postId, memberId]
      );
      if (insertResult.affectedRows > 0) {
        return res.json({
          success: true,
        });
      } else
        return res.json({
          success: false,
          message: "can't insert into history record",
        });
    } else {
      const [updateResult, updateFields] = await db().execute(
        "update history set history_date = now() where history_post_id =? and history_member_id = ?",
        [postId, memberId]
      );
      if (updateResult.affectedRows > 0) {
        return res.json({
          success: true,
          message: "date of this record is updated",
        });
      } else
        return res.json({
          success: false,
          message: "can't update the date of this record",
        });
    }
  } catch (error) {
    res.json(error);
  }
});

router.get("/history", verify, async (req, res) => {
  try {
    let memberId = parseInt(req.memberId);
    const numberPerPage = 15;
    const pageNumber = parseInt(req.query.page);
    const offset = (pageNumber - 1) * numberPerPage;
    const sql = `with h as (select * from history where history_member_id = ${memberId} limit ${numberPerPage} offset ${offset}),
    postlist as (select m.username, p.*,h.history_date from post p 
                left join member m on m.member_id = p.author
                inner join h on h.history_post_id = p.post_id),
                commentlist as (select count(c.comment_id) as number_of_comment, 
                c.post_id, ceil(count(c.comment_id)/15) as sum_of_comment_page 
                from comment c inner join postlist on postlist.post_id = c.post_id group by c.post_id)
    select p.*, case when c.sum_of_comment_page is null then 1 else c.sum_of_comment_page end
                as sum_of_comment_page from postlist p left join commentlist c on p.post_id = c.post_id
                order by p.history_date desc`;

    const [result, fields] = await db().execute(sql);
    const [sumResult, sumFields] = await db().execute(
      "select ceil(count(*)/15) as sum_of_page from history where history_member_id = ?",
      [memberId]
    );
    let sumOfPage = sumResult[0].sum_of_page;
    return res.json({ result: result, sumOfPage: sumOfPage });
  } catch (error) {
    res.json({ error });
  }
});

router.post("/bookmark", verify, async (req, res) => {
  try {
    const postId = req.body.postId;
    const markDate = new Date();
    const memberId = parseInt(req.memberId);
    const [result] = await db().execute("select * from post where post_id=?", [
      postId,
    ]);
    console.log(result);
    if (result.length > 0) {
      const [isBookmarked] = await db().execute(
        "select * from bookmark where post_id=?",
        [postId]
      );

      if (isBookmarked.length === 0) {
        const [outcome] = await db().execute(
          "insert into bookmark(post_id,member_id,mark_date)values(?,?,?)",
          [postId, memberId, markDate]
        );
        res.json({
          success: true,
          message: "post " + postId + " has been bookmarked",
        });
      } else {
        const [delBookmark] = await db().execute(
          "delete from bookmark where post_id=?",
          [postId]
        );
        res.json({
          success: true,
          message: "delete bookmark success",
        });
      }
    } else {
      res.json({
        success: false,
        message: "doesn't exist " + postId,
      });
    }
  } catch (err) {
    res.json(err);
    console.log(err);
  }
});

router.get("/bookmark/page/:page", verify, async (req, res) => {
  try {
    const memberId = parseInt(req.memberId);
    const numberPerPage = 15;
    const pageNumber = parseInt(req.params.page);
    const offset = (pageNumber - 1) * numberPerPage;
    const [result] = await db()
      .execute(`with h as (select * from bookmark where member_id = ${memberId} limit ${numberPerPage} offset ${offset}),
  postlist as (select m.username, p.*,h.mark_date from post p 
              left join member m on m.member_id = p.author
              inner join h on h.post_id = p.post_id),
              commentlist as (select count(c.comment_id) as number_of_comment, 
              c.post_id, ceil(count(c.comment_id)/15) as sum_of_comment_page 
              from comment c inner join postlist on postlist.post_id = c.post_id group by c.post_id)
  select p.*, case when c.sum_of_comment_page is null then 1 else c.sum_of_comment_page end
              as sum_of_comment_page from postlist p left join commentlist c on p.post_id = c.post_id
              order by p.mark_date desc`);
    const [sumResult, sumFields] = await db().execute(
      `select ceil(count(*)/15) as sum_of_page from bookmark where member_id  = ${memberId}`
    );

    return res.json({
      result: result,
      sumOfPage: parseInt(sumResult[0].sum_of_page),
    });
  } catch (err) {
    res.json(err);
    console.log(err);
  }
});
module.exports = router;

const express = require('express');
const mysql = require('mysql2');
const db = require("../db.js").getDb;
let router = express.Router();
const app = express();
const verify = require('../tools/verify');

app.use(express.json());

router.post("/:id/like", verify,async (req, res) => {
    try {
        let id = parseInt(req.params.id);
        let memberId = req.body.memberId;
        const [findPostResult, findPostFields] = await db().execute('select post_id from post where post_id =?', [id]);
        if (findPostResult.length === 0) {
            return res.json({ success:false, error: `Post:${id} is not found.` });
        }
        const [result, fields] = await db().execute('select * from member_post_like where like_post_id =? and like_member_id =? and islike = true', [id, memberId]);
        if (result.length === 0) {
            const [insertResult, insertFields] = await db().execute('insert into member_post_like (like_post_id, like_member_id,islike) values (?,?,true)', [id, memberId]);
            if (insertResult.affectedRows > 0) {
                const [addLikeToPostResult, addLikeToPostFields] = await db().execute('update post set like_count = like_count+1 where post_id = ?', [id]);
                return res.json({
                    success: true,
                    message:'like is added'
                });
            } else {
                return res.json({
                    success: false,
                    message: "failed to like the post"
                });
            };
        } else {
            const [deleteResult, deleteFields] = await db().execute('delete from member_post_like where like_post_id =? and like_member_id = ? and islike = true', [id, memberId]);
            if (deleteResult.affectedRows > 0) {
                const [removeLikeFromPostResult, removeLikeFromPostFields] = await db().execute('update post set like_count = like_count-1 where post_id = ?', [id]);
                return res.json({
                    success: true,
                    message:'like is cancelled'
                })
            } else {
                return res.json({
                    success:false,
                    message: 'failed to cancel like'
                });
            }
        };
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
    }
});

router.post("/:id/dislike", verify, async (req, res) => {
    try {
        let id = parseInt(req.params.id);
        let memberId = req.body.memberId;
        const [findPostResult, findPostFields] = await db().execute('select post_id from post where post_id =?', [id]);
        if (findPostResult.length === 0) {
            return res.json({ success:false, error: `Post:${id} is not found.` });
        }
        const [result, fields] = await db().execute('select * from member_post_like where like_post_id =? and like_member_id =? and islike = false', [id, memberId]);
        if (result.length === 0) {
            const [insertResult, insertFields] = await db().execute('insert into member_post_like (like_post_id, like_member_id,islike) values (?,?,false)', [id, memberId]);
            if (insertResult.affectedRows > 0) {
                const [addDislikeToPostResult, addDislikeToPostFields] = await db().execute('update post set dis_count = dis_count+1 where post_id = ?', [id]);
                return res.json({
                    success: true,
                    message:'Dislike is added'
                });
            } else {
                return res.json({
                    success: false,
                    message: "failed to dislike the post"
                });
            };
        } else {
            const [deleteResult, deleteFields] = await db().execute('delete from member_post_like where like_post_id =? and like_member_id = ? and islike = false', [id, memberId]);
            if (deleteResult.affectedRows > 0) {
                const [removeDislikeFromPostResult, removeDislikeFromPostFields] = await db().execute('update post set dis_count = dis_count-1 where post_id = ?', [id]);
                return res.json({
                    success: true,
                    message:'dislike is cancelled'
                })
            } else {
                return res.json({
                    success:false,
                    message: 'failed to cancel dislike'
                });
            }
        };
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
    }
});

module.exports = router;
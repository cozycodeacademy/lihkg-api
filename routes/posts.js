const express = require("express");
const mysql = require("mysql2");
const multer = require("multer");
const upload = multer({ dest: "uploaded-picture/" });
const { Router } = require("express");
const router = express.Router();
const db = require("../db").getDb;
const verify = require("../tools/verify");
const { captureRejectionSymbol } = require("events");
const e = require("express");

router.get("/search", async (req, res) => {
  const numberPerPage = 15;
  const pageNumber = parseInt(req.query.page);
  const offset = (pageNumber - 1) * numberPerPage;

  //post less than 1 hour won't show like and dislike
  function filteredResult(data) {
    return data.map((data) => {
      if ((Date.now() - data.date.getTime()) / 3600000 < 1) {
        data.like_count = -1;
        data.dis_count = -1;
        return data;
      } else return data;
    });
  }

  if (req.query.postname && req.query.page) {
    let postName = `'%${req.query.postname}%'`;
    let sql = `with postlist as (select m.username, p.* from post p 
        left join member m on m.member_id = p.author 
        where name like ${postName} limit ${numberPerPage} offset ${offset}),
        commentlist as (select count(c.comment_id) as number_of_comment, 
        c.post_id, ceil(count(c.comment_id)/15) as sum_of_comment_page 
        from comment c inner join postlist on postlist.post_id = c.post_id group by c.post_id)
        select p.*, case when c.sum_of_comment_page is null then 1 else c.sum_of_comment_page end
        as sum_of_comment_page from postlist p left join commentlist c on p.post_id = c.post_id`;
    const [result, fields] = await db().execute(sql);
    let filteredData = filteredResult(result);
    const [sumResult, sumFields] = await db().execute(
      `select ceil(count(*)/15) as sum_of_page from post where name like ${postName}`
    );

    return res.json({
      result: filteredData,
      sumOfPage: parseInt(sumResult[0].sum_of_page),
    });
  }

  if (req.query.userid && req.query.page) {
    let author = parseInt(req.query.userid);
    let sql = `with postlist as (select m.username, p.* from post p 
        left join member m on m.member_id = p.author 
        where author = ${author} limit ${numberPerPage} offset ${offset}),
        commentlist as (select count(c.comment_id) as number_of_comment, 
        c.post_id, ceil(count(c.comment_id)/15) as sum_of_comment_page 
        from comment c inner join postlist on postlist.post_id = c.post_id group by c.post_id)
        select p.*, case when c.sum_of_comment_page is null then 1 else c.sum_of_comment_page end
        as sum_of_comment_page from postlist p left join commentlist c on p.post_id = c.post_id`;
    const [result, fields] = await db().execute(sql);
    let filteredData = filteredResult(result);
    const [sumResult, sumFields] = await db().execute(
      `select ceil(count(*)/15) as sum_of_page from post where author = ${author}`
    );

    return res.json({
      result: filteredData,
      sumOfPage: parseInt(sumResult[0].sum_of_page),
    });
  }

  if (req.query.category && req.query.page) {
    const category = req.query.category;
    const sql = `with postlist as (select m.username, p.* from post p 
            left join member m on m.member_id = p.author 
            where category = '${category}' order by date desc limit ${numberPerPage} offset ${offset}),
            commentlist as (select count(c.comment_id) as number_of_comment, 
            c.post_id, ceil(count(c.comment_id)/15) as sum_of_comment_page 
            from comment c inner join postlist on postlist.post_id = c.post_id group by c.post_id)
            select p.*, case when c.sum_of_comment_page is null then 1 else c.sum_of_comment_page end
            as sum_of_comment_page from postlist p left join commentlist c on p.post_id = c.post_id`;
    const [result, fields] = await db().execute(sql);
    let filteredData = filteredResult(result);
    const [sumResult, sumFields] = await db().execute(
      "select ceil(count(*)/15) as sum_of_page from post where category =?",
      [category]
    );
    return res.json({
      result: filteredData,
      sumOfPage: parseInt(sumResult[0].sum_of_page),
    });
  }
});

router.get("/:postId/page/:page", async (req, res) => {
  try {
    const numberPerPage = 15;
    const pageNumber = parseInt(req.params.page);
    const offset = (pageNumber - 1) * numberPerPage;
    const postId = parseInt(req.params.postId);

    const [row, files] = await db().execute(
      "select * from post where post_id = ?",
      [postId]
    );

    if (row.length > 0) {
      const sql = `select m.username, p.* from post p 
      left join member m on m.member_id = p.author
      where p.post_id = ${postId}`;
      const [postResult, fields] = await db().execute(sql);
      if ((Date.now() - row[0].date.getTime()) / 3600000 < 1) {
        postResult[0].like_count = -1;
        postResult[0].dis_count = -1;
      }
      const post_pic = `select pp.* from post_picture pp 
        inner join post p on p.post_id = pp.post_id
        where p.post_id = ${postId}`;
      const [postPicResult, postPicfields] = await db().execute(post_pic);
      let post_pic_ary = [];
      for (let i = 0; i < postPicResult.length; i++) {
        post_pic_ary.push(postPicResult[i].path);
      }
      postResult[0]["pic_path"] = post_pic_ary;

      const comment_sql = `select m.username, c.* from comment c
        left join member m on m.member_id = c.commentor
        where c.post_id = ${postId} order by date asc limit ${numberPerPage} offset ${offset}`;
      const [commentResult, commentfields] = await db().execute(comment_sql);

      const comment_pic_sql = `with temp as (select comment_id from comment 
            where post_id = ${postId} order by date asc limit ${numberPerPage} offset ${offset})
            select cp.* from comment_picture cp
            inner join temp on temp.comment_id = cp.comment_id`;
      const [commentPicResult, commentPicfields] = await db().execute(
        comment_pic_sql
      );
      let ary = [];
      for (let i = 0; i < commentPicResult.length; i++) {
        let found = ary.findIndex(
          (e) => e.comment_id === commentPicResult[i].comment_id
        );
        if (found > -1) {
          ary[found].path.push(commentPicResult[i].path);
        } else {
          ary.push({
            comment_id: commentPicResult[i].comment_id,
            path: [commentPicResult[i].path],
          });
        }
      }

      for (let i = 0; i < commentResult.length; i++) {
        let found = ary.findIndex(
          (e) => e.comment_id === commentResult[i].comment_id
        );
        if (found > -1) {
          commentResult[i]["pic_path"] = ary[found].path;
        } else {
          commentResult[i]["pic_path"] = null;
        }
      }

      const [sumResult, sumFields] = await db().execute(
        `select ceil(count(*)/15) as sum_of_page from comment where post_id = ${postId}`
      );

      if (parseInt(sumResult[0].sum_of_page) === 0) {
        sumResult[0].sum_of_page = 1;
      }
      res.json({
        post: postResult,
        comment: commentResult,
        sumOfPage: parseInt(sumResult[0].sum_of_page),
      });
    } else {
      res.json({
        success: false,
        message: "this service is not found",
      });
    }
  } catch (error) {
    res.json(error);
  }
});

router.post("/:id/like", verify, async (req, res) => {
  try {
    let id = parseInt(req.params.id);
    let memberId = req.memberId;
    const [findPostResult, findPostFields] = await db().execute(
      "select post_id from post where post_id =?",
      [id]
    );
    if (findPostResult.length === 0) {
      return res.json({ success: false, message: `Post:${id} is not found.` });
    }
    const [result, fields] = await db().execute(
      "select * from member_post_like where like_post_id =? and like_member_id =?",
      [id, memberId]
    );
    if (result.length === 0) {
      const [insertResult, insertFields] = await db().execute(
        "insert into member_post_like (like_post_id, like_member_id,islike) values (?,?,true)",
        [id, memberId]
      );
      if (insertResult.affectedRows > 0) {
        const [addLikeToPostResult, addLikeToPostFields] = await db().execute(
          "update post set like_count = like_count+1 where post_id = ?",
          [id]
        );
        return res.json({
          success: true,
          message: "add like success",
        });
      } else {
        return res.json({
          success: false,
          message: "failed to like the post",
        });
      }
    } else {
      if (result[0].islike === 1) {
        const [deleteResult, deleteFields] = await db().execute(
          "delete from member_post_like where like_post_id =? and like_member_id = ? and islike = true",
          [id, memberId]
        );
        if (deleteResult.affectedRows > 0) {
          const [removeLikeFromPostResult, removeLikeFromPostFields] =
            await db().execute(
              "update post set like_count = like_count-1 where post_id = ?",
              [id]
            );
          return res.json({
            success: true,
            message: "like is cancelled",
          });
        } else {
          return res.json({
            success: false,
            message: "failed to cancel like",
          });
        }
      } else {
        return res.json({
          success: false,
          message: "you have to cancel the dislike first",
        });
      }
    }
  } catch (error) {
    res.json({
      success: false,
      message: error,
    });
  }
});

router.post("/:id/dislike", verify, async (req, res) => {
  try {
    let id = parseInt(req.params.id);
    let memberId = req.memberId;
    const [findPostResult, findPostFields] = await db().execute(
      "select post_id from post where post_id =?",
      [id]
    );
    if (findPostResult.length === 0) {
      return res.json({ success: false, error: `Post:${id} is not found.` });
    }
    const [result, fields] = await db().execute(
      "select * from member_post_like where like_post_id =? and like_member_id =?",
      [id, memberId]
    );
    if (result.length === 0) {
      const [insertResult, insertFields] = await db().execute(
        "insert into member_post_like (like_post_id, like_member_id,islike) values (?,?,false)",
        [id, memberId]
      );
      if (insertResult.affectedRows > 0) {
        const [addDislikeToPostResult, addDislikeToPostFields] =
          await db().execute(
            "update post set dis_count = dis_count+1 where post_id = ?",
            [id]
          );
        return res.json({
          success: true,
          message: "Dislike is added",
        });
      } else {
        return res.json({
          success: false,
          message: "failed to dislike the post",
        });
      }
    } else {
      if (result[0].islike === 0) {
        const [deleteResult, deleteFields] = await db().execute(
          "delete from member_post_like where like_post_id =? and like_member_id = ? and islike = false",
          [id, memberId]
        );
        if (deleteResult.affectedRows > 0) {
          const [removeDislikeFromPostResult, removeDislikeFromPostFields] =
            await db().execute(
              "update post set dis_count = dis_count-1 where post_id = ?",
              [id]
            );
          return res.json({
            success: true,
            message: "dislike is cancelled",
          });
        } else {
          return res.json({
            success: false,
            message: "failed to cancel dislike",
          });
        }
      } else {
        return res.json({
          success: false,
          message: "you have to cancel the like first",
        });
      }
    }
  } catch (error) {
    res.json({
      success: false,
      message: error,
    });
  }
});

router.post(
  "/:postId/photo",
  verify,
  upload.array("picture", 5),
  async (req, res) => {
    try {
      const memberId = parseInt(req.memberId);
      // const memberId = 1;
      const postId = req.params.postId;
      const picture = req.files;

      //find member
      const [memberIsThere, fields] = await db().execute(
        `select * from member where member_id = ?`,
        [memberId]
      );
      if (!memberIsThere) {
        return res.json({
          success: false,
          message: "the member is not found",
        });
      }
      //find post
      const [postIsThere, fields2] = await db().execute(
        `select * from post where post_id = ?`,
        [postId]
      );
      if (postIsThere.length === 0) {
        return res.json({
          success: false,
          message: "the post is not found",
        });
      }
      //confirm the post's author
      const [myPost, fields3] = await db().execute(
        `select * from post where author=? and post_id=?`,
        [memberId, postId]
      );
      if (myPost.length === 0) {
        return res.json({
          success: false,
          message: "the post's author is not you, so you cant upload pictures",
        });
      }
      //confirm the picture's extension
      function checkPictureExtension() {
        console.log(picture)
        for (let i = 0; i < picture.length; i++) {
          if (
            picture[i].mimetype !== "image/png" &&
            picture[i].mimetype !== "image/jpeg" &&
            picture[i].mimetype !== "image/webp" &&
            picture[i].mimetype !== "image/gif"
          ) {
            return false;
          }
        }
        return true;
      }
      let correctExtension = checkPictureExtension(picture);
      if (!correctExtension) {
        return res.json({
          success: false,
          message: "some picture extension are not png, jpg, webp, gif",
        });
      }
      //check the picture size
      function checkPictureSize() {
        for (let i = 0; i < picture.length; i++) {
          if (picture[i].size > 400000) {
            return false;
          }
        }
        return true;
      }
      let correctSize = checkPictureSize(picture);
      if (!correctSize) {
        return res.json({
          success: false,
          message: "some picture size is more than 400kb",
        });
      }
      //update picture
      async function updatePicture() {
        let updateNum = 0;
        for (let i = 0; i < picture.length; i++) {
          let str=picture[i].path;
          let res="/"+str.replace(/\\/g,"/")
          const [row, fields3] = await db().execute(
            `insert into post_picture(path, post_id) values(?,?)`,
            [res, postId]
          );
          updateNum = updateNum + 1;
        }
        return updateNum;
      }
      const updatePicturesNum = await updatePicture(picture);
      if (picture.length === updatePicturesNum) {
        return res.json({
          success: true,
          message: "success to update all pictures",
        });
      } else {
        return res.json({
          success: false,
          message: "fail to update pictures",
        });
      }
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  }
);

router.post("", verify, async (req, res) => {
  try {
    let lt = /</g,
      gt = />/g,
      ap = /'/g,
      ic = /"/g;
    const name = req.body.name;
    const content = req.body.content
      .replace(lt, "&lt;")
      .replace(gt, "&gt;")
      .replace(ap, "&#39;")
      .replace(ic, "&#34;");
    const contentLength = content.split(" ").join("").length;
    const createTime = new Date();
    const author = parseInt(req.memberId);
    const private = req.body.private;
    let outcome = null;
    const category = req.body.category;

    if (contentLength <= 100) {
      const [result, field] = await db().execute(
        "insert into post(author,date,content,like_count,dis_count,private,name,category)values(?,?,?,0,0,?,?,?)",
        [author, createTime, content, private, name, category]
      );
      outcome = result;
    } else {
      res.json({ message: "max lenth 100 words" });
    }
    if (outcome.affectedRows > 0) {
      res.json({
        success: true,
        id: outcome.insertId,
      });
    } else {
      res.json({
        success: false,
        message: "fail to create ",
      });
    }
  } catch (err) {
    res.json(err);
    console.log(err)
  }
});

module.exports = router;
